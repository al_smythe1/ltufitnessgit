package com.example.ltufitness1;

import static org.junit.Assert.*;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.test.platform.app.InstrumentationRegistry;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

public class DBHandlerTest{
    DBHandler handler;
    private void databaseStart(){
        handler = new DBHandler(InstrumentationRegistry.getInstrumentation().getTargetContext());
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
    }
    private void createTestUser(){
        User testMyUser = new User("username","1234","username","09898182736","yoga","yoga");
        handler.insertData(testMyUser);
    }
    @Test
    public void testInsertData() {
        databaseStart();
        User testMyUser = new User("username","1234","username","09898182736","yoga","yoga");
        assertTrue(handler.insertData(testMyUser));
        handler.endTest();
        handler = null;
    }
    @Test
    public void testCheckUserExists() {
        databaseStart();
        createTestUser();
        assertTrue(handler.checkUserExists("username","1234"));
        handler.endTest();
        handler = null;
    }
    @Test
    public void testCheckUserNoPass() {
        databaseStart();
        createTestUser();
        assertTrue(handler.checkUserNoPass("username"));
        handler.endTest();
        handler = null;
    }
    @Test
    public void testCheckClassLink() {
        databaseStart();
        createTestUser();
        assertFalse(handler.checkClassLink("username"));
        handler.endTest();
        handler = null;
    }
    @Test
    public void testRemoveUserAccount() {
        databaseStart();
        createTestUser();
        assertTrue(handler.removeUserAccount("username"));
        handler.endTest();
        handler = null;
    }
    @Test
    public void testCheckUsersIndividualClass() {
        databaseStart();
        createTestUser();
        handler.settingClassLink("username","1",true);
        assertTrue(handler.checkUsersIndividualClass("username","1"));
        handler.endTest();
        handler = null;
    }
    @Test
    public void testSettingClassLink() {
        databaseStart();
        createTestUser();
        handler.settingClassLink("username","1",true);
        assertTrue(handler.checkUsersIndividualClass("username","1"));
        handler.endTest();
        handler = null;
    }
}