package com.example.ltufitness1;

public class User {
    private String userName;
    private String userPassword;
    private String userEmail;
    private String userNumber;
    private String userprefclass;
    private String userprefsport;

    public User(String userName, String userPassword, String userEmail, String userNumber, String userprefclass, String userprefsport ){
        this.userName = userName;
        this.userPassword = userPassword;
        this.userEmail = userEmail;
        this.userNumber = userNumber;
        this.userprefclass = userprefclass;
        this.userprefsport = userprefsport;
    }

    public String getUserName() {
        return this.userName;
    }
    public String getUserPassword() {
        return this.userPassword;
    }
    public String getUserEmail() {
        return this.userEmail;
    }
    public String getUserNumber() {
        return this.userNumber;
    }
    public String getUserprefclassl() {
        return this.userprefclass;
    }
    public String getUserprefsport() {
        return this.userprefsport;
    }
}
