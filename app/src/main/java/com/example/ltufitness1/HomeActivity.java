package com.example.ltufitness1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class HomeActivity extends AppCompatActivity {

    String username;
    DBHandler handler;
    Context thisActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent intusername = getIntent();
        username = intusername.getStringExtra("username");
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        classList();
    }
    public void classList(){
        String[] listClass = handler.getclasses();
        LinearLayout displayClassLayout = findViewById(R.id.app_home_scrollview_linearlayout);
        ScrollView displayClassScrollview = findViewById(R.id.app_home_scrollview);
        ViewGroup.LayoutParams layoutForLinear = displayClassLayout.getLayoutParams();
        ViewGroup.LayoutParams layoutForScrollView = displayClassScrollview.getLayoutParams();
        Button[] buttonList = new Button[listClass.length];
        int count = 0;
        while (count < listClass.length){
            buttonList[count] = new Button(this);
            buttonList[count].setHeight(50);
            buttonList[count].setWidth(50);
            buttonList[count].setTextColor(Color.WHITE);
            buttonList[count].setText(handler.getClassName(String.valueOf(count + 1)));
            buttonList[count].setId(count + 1);
            if (handler.checkUsersIndividualClass(username, String.valueOf(count))){
                buttonList[count].setBackgroundColor(Color.GREEN);
                buttonList[count].setTextColor(Color.BLACK);
            }
            else{
                buttonList[count].setBackgroundColor(Color.RED);
                buttonList[count].setTextColor(Color.WHITE);
            }
            buttonList[count].setOnClickListener(bookClassListener);
            LinearLayout.LayoutParams buttonLayout = new LinearLayout.LayoutParams(800,300);
            buttonLayout.setMargins(0,0,0,50);
            buttonList[count].setLayoutParams(buttonLayout);
            displayClassLayout.addView(buttonList[count]);
            displayClassLayout.setLayoutParams(layoutForLinear);
            displayClassScrollview.setLayoutParams(layoutForScrollView);
            count++;
        }
    }
    public void searchButton(View view){
        EditText searchbox = findViewById(R.id.app_bookclass_edittext_searchtext);
        Intent searchswitch = new Intent(this, SearchActivity.class);
        searchswitch.putExtra("search", String.valueOf(searchbox.getText()));
        searchswitch.putExtra("username",username);
        startActivity(searchswitch);
    }
    View.OnClickListener bookClassListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            Intent classsinfo = new Intent(thisActivity,ClassActivity.class);
            classsinfo.putExtra("class_id", String.valueOf(view.getId()));
            classsinfo.putExtra("username",username);
            startActivity(classsinfo);
        }
    };
    public void myProfileButton(View view){
        Intent back = new Intent(this,ProfileActivity.class);
        startActivity(back);
        finish();
    }
}