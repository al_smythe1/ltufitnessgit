package com.example.ltufitness1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHandler extends SQLiteOpenHelper {
    static SQLiteDatabase db;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "fitnessdatabase.db";

    private static final String CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS users_table" +
            "(user_id integer PRIMARY KEY NOT NULL," +
            "username text NOT NULL," +
            "password text NOT NULL," +
            "email text NOT NULL," +
            "number text," +
            "prefclass text," +
            "prefsport text);";

    private static final String CREATE_BOOKINGS_TABLE = "CREATE TABLE IF NOT EXISTS bookings_table" +
            "(bookings_id integer PRIMARY KEY NOT NULL," +
            "class_name text NOT NULL," +
            "bookings_date text NOT NULL," +
            "bookings_capacity text NOT NULL," +
            "bookings_time text NOT NULL);";

    private static final String LINKS_BOOKINGUSER_TABLE = "CREATE TABLE IF NOT EXISTS bookinguser_link" +
            "(user_id integer NOT NULL," +
            "bookings_id integer NOT NULL," +
            "PRIMARY KEY (user_id,bookings_id)," +
            "FOREIGN KEY(user_id) REFERENCES users_table(user_id)," +
            "FOREIGN KEY(bookings_id) REFERENCES bookings_table(bookings_id));";

    public DBHandler(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL(CREATE_USERS_TABLE);
        db.execSQL(CREATE_BOOKINGS_TABLE);
        db.execSQL(LINKS_BOOKINGUSER_TABLE);
        this.db = db;
        addClassDB();
    }

    public boolean insertData(User user){
        ContentValues values = new ContentValues();
        values.put("username", user.getUserName());
        values.put("password", user.getUserPassword());
        values.put("email", user.getUserEmail());
        values.put("number", user.getUserNumber());
        values.put("prefclass", user.getUserprefclassl());
        values.put("prefsport", user.getUserprefsport());
        try{
            db.insert("users_table",null,values);
            Log.d("insertData","User inserted SUCCESSFULLY");
            return true;
        }
        catch(Exception error){
            Log.d("insertData","user inserted UNSUCCESSFULLY");
            return false;
        }
    }
    public boolean checkUserExists(String username, String password){
        Cursor cursor = db.rawQuery("SELECT email, password FROM users_table",null);
        if (cursor.moveToFirst()){
            do {
                if (cursor.getString(0).equals(username)){
                    if (cursor.getString(1).equals(password)){
                        return true;
                    }
                }
            }
            while (cursor.moveToNext());
        }
        return false;
    }
    public boolean checkUserNoPass(String username){
        Cursor cursor = db.rawQuery("SELECT username FROM users_table",null);
        if (cursor.moveToFirst()){
            do {
                if (cursor.getString(0).equals(username)){
                    return true;
                }
            }
            while (cursor.moveToNext());
        }
        return false;
    }
    public String getUserId(String username){
        Cursor cursor = db.rawQuery("SELECT user_id FROM users_table WHERE email = '" + username + "'",null);
        if (cursor.moveToFirst()){
            return cursor.getString(0);
        }
        return "NoUser";
    }
    public boolean checkClassLink(String username){
        String user_id = getUserId(username);
        Cursor cursor = db.rawQuery("SELECT user_id FROM bookinguser_link WHERE user_id = " + user_id, null);
        if (cursor.getCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean removeUserAccount(String username){
        String user_id =getUserId(username);
        try{
            if (checkClassLink(username)){
                db.delete("bookinguser_link", "user_id = " + user_id, null);
            }
            db.delete("users_table", "user_id =" + user_id, null );
            return true;
        }
        catch (Exception error){
            return false;
        }
    }
    public boolean checkUsersIndividualClass(String username, String classId){
        String user_id = getUserId(username);
        Cursor cursor = db.rawQuery("SELECT user_id FROM bookinguser_link WHERE user_id = " + user_id + " AND bookings_id = " + classId, null);
        if (cursor.getCount() > 0){
            return true;
        }
        else{
            return false;
        }
    }
    public boolean settingClassLink(String username, String classId, boolean bookingLink){
        String user_id = getUserId(username);
        if (bookingLink){
            ContentValues insertClasses = new ContentValues();
            insertClasses.put("user_id",user_id);
            insertClasses.put("bookings_id",classId);
            db.insert("bookinguser_link", null,insertClasses);
            return true;
        }
        else {
            db.delete("bookinguser_link","user_id = " + user_id + "AND bookings_id = " + classId, null);
            return false;
        }
    }
    public String[] getclasses(){
        Cursor cursor = db.rawQuery("SELECT bookings_id FROM bookings_table", null);
        String[] returnClasses = new String[cursor.getCount()];
        int counter = 0;
        if (cursor.moveToFirst()){
            while(cursor.moveToNext() && counter <= cursor.getCount()){
                returnClasses[counter] = cursor.getString(0);
                counter++;
            }
        }
        return returnClasses;
    }
    public String getClassName(String classId){
        Cursor cursor = db.rawQuery("SELECT class_name FROM bookings_table WHERE bookings_id = " + classId, null);
        if (cursor.moveToFirst()){
            return cursor.getString(0);
        }
        else {
            return "NoClass";
        }
    }
    public String getclassinfo(String classId){
        Cursor cursor = db.rawQuery("SELECT class_name, bookings_date, bookings_capacity,bookings_time FROM bookings_table WHERE bookings_id =" + classId, null);
        if (cursor.moveToFirst()){
            return cursor.getString(0) + "\n" + cursor.getString(1) +"\n" + cursor.getString(2) +"\n" + cursor.getString(3);
        }
        else{
            return "NoClass";
        }
    }
    public String[] classSearch(String search){
        Cursor cursor = db.rawQuery("SELECT bookings_id, class_name FROM bookings_table WHERE class_name LIKE '" + search + "%'", null);
        String[] searchResults = new String[cursor.getCount()];
        if (cursor.moveToFirst()){
            int count = 0;
            do{
                searchResults[count] = cursor.getString(1);
                searchResults[count] = searchResults[count] + "/" + cursor.getString(0);
                count++;
            }
            while(count <= cursor.getCount() && cursor.moveToNext());
        }
        return searchResults;
    }
    public void addClassDB(){
        db.delete("bookings_table","1=1",null);
        ContentValues dataStorage = new ContentValues();
        dataStorage.put("class_name","Boxing");
        dataStorage.put("bookings_date","03/03/2029");
        dataStorage.put("bookings_capacity","10");
        dataStorage.put("bookings_time","10:00");
        db.insert("bookings_table",null, dataStorage);

        dataStorage.put("class_name","Cardio");
        dataStorage.put("bookings_date","04/03/2029");
        dataStorage.put("bookings_capacity","10");
        dataStorage.put("bookings_time","20:00");
        db.insert("bookings_table",null, dataStorage);

        dataStorage.put("class_name","Strength Training");
        dataStorage.put("bookings_date","05/03/2029");
        dataStorage.put("bookings_capacity","10");
        dataStorage.put("bookings_time","12:00");
        db.insert("bookings_table",null, dataStorage);

        dataStorage.put("class_name","Yoga");
        dataStorage.put("bookings_date","07/03/2029");
        dataStorage.put("bookings_capacity","10");
        dataStorage.put("bookings_time","13:00");
        db.insert("bookings_table",null, dataStorage);
    }
    public void endTest(){
        db.delete("bookinguser_link", "1=1",null);
        db.delete("users_table", "1=1",null);
        db.delete("bookings_table", "1=1",null);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        String dropCommand = "DROP TABLE IF EXISTS users_table";
    }

}
