package com.example.ltufitness1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ClassActivity extends AppCompatActivity {
    DBHandler handler;
    String username;
    String classData;
    String classId;

    Button bookClass;
    TextView seeclassinfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class);
        Intent intusername = getIntent();
        username = intusername.getStringExtra("username");
        classId = intusername.getStringExtra("class_id");
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        classData = handler.getclassinfo(classId);
        seeclassinfo = findViewById(R.id.app_bookclassact_textview_displayinfo);
        bookClass = findViewById(R.id.app_classact_bookclassbutton);
        seeclassinfo.setText(classData);
    }
    public void setBookClass(View view){
        if (handler.checkUsersIndividualClass(username,classId)){
            handler.settingClassLink(username,classId, false);
        }
        else {
            handler.settingClassLink(username,classId, true);
        }
    }
    public void backButton(View view){
        Intent back = new Intent(this,HomeActivity.class);
        startActivity(back);
        finish();
    }
}