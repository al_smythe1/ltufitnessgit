package com.example.ltufitness1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class SearchActivity extends AppCompatActivity {

    DBHandler handler;
    
    String username;
    
    String search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Intent intusername = getIntent();
        username = intusername.getStringExtra("username");
        search = intusername.getStringExtra("search");
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
        listSearch();
    }
    public void listSearch(){
        String[] searchresults = handler.classSearch(search);
        String[] searchresultssplit = new String[2];
        ScrollView displaySearchScrollView = findViewById(R.id.app_search_scroll_scrollview);
        LinearLayout displaySearchLayout = findViewById(R.id.app_search_scroll_scrollviewlinear);
        ViewGroup.LayoutParams layoutForLinear = displaySearchLayout.getLayoutParams();
        ViewGroup.LayoutParams layoutForScrollView = displaySearchScrollView.getLayoutParams();
        Button[] buttonList = new Button[searchresults.length];
        int count = 0;
        while (count < searchresults.length){
            Log.d("cake",searchresults[count]);
            searchresultssplit = searchresults[count].split("/");
            buttonList[count] = new Button(this);
            buttonList[count].setHeight(50);
            buttonList[count].setWidth(50);
            buttonList[count].setTextColor(Color.WHITE);
            buttonList[count].setBackgroundColor(Color.BLACK);
            buttonList[count].setText(searchresultssplit[0]);
            buttonList[count].setId(Integer.parseInt(searchresultssplit[1]));
            LinearLayout.LayoutParams buttonLayout = new LinearLayout.LayoutParams(800,300);
            buttonLayout.setMargins(0,0,0,50);
            buttonList[count].setLayoutParams(buttonLayout);
            displaySearchLayout.addView(buttonList[count]);
            displaySearchLayout.setLayoutParams(layoutForLinear);
            displaySearchScrollView.setLayoutParams(layoutForScrollView);
            count++;
        }
    }
}