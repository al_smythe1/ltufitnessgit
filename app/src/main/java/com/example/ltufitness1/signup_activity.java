package com.example.ltufitness1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class signup_activity extends AppCompatActivity {
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
    }
    public void signupButton(View view){
        EditText email = findViewById(R.id.app_signupact_editText_email);
        EditText password = findViewById(R.id.app_signupact_edittext_editpassword);
        EditText username = findViewById(R.id.app_signupact_edittext_editusername);
        EditText number  = findViewById(R.id.app_signupact_edittext_number);
        EditText prefclass  = findViewById(R.id.app_signupact_edittext_editprefclass);
        EditText prefsport  = findViewById(R.id.app_signupact_edittext_editprefsport);
        if (String.valueOf(email.getText()).equals("")){
            email.setError(getResources().getString(R.string.app_signupact_text_emailEmpty));
            email.requestFocus();
            return;
        }
        else if (String.valueOf(password.getText()).equals("") || (String.valueOf(password.getText()).length() < 6)){
            if (String.valueOf(password.getText()).equals("")){
                password.setError(getResources().getString(R.string.app_signupact_text_passwordEmpty));
            }
            else if (String.valueOf(password.getText()).length() < 6){
                password.setError(getResources().getString(R.string.app_signupact_text_passwordincomplete));
            }
            password.requestFocus();
            return;
        }
        else if (String.valueOf(username.getText()).equals("")) {
            Log.d("auth",String.valueOf(username.getText()));
            username.setError(getResources().getString(R.string.app_signupact_text_usernameEmpty));
            username.requestFocus();
            return;
        }
        else{
            if (!handler.checkUserNoPass(String.valueOf(username.getText()))){
                String mobileText = String.valueOf(number.getText());
                String prefClass = String.valueOf(prefclass.getText());
                String prefSport = String.valueOf(prefsport.getText());
                if (mobileText.equals("")){
                    mobileText = "no mobile";
                }
                if (prefClass.equals("")){
                    prefClass = "no preferred class";
                }
                if (prefSport.equals("")){
                    prefSport = "no preferred sport";
                }
                User user = new User(String.valueOf(username.getText()), String.valueOf(password.getText()), String.valueOf(email.getText()),mobileText,prefClass,prefSport);
                handler.insertData(user);
                backButton(view);
            }
        }
    }

    public void backButton(View view){
        Intent back = new Intent(this,MainActivity.class);
        startActivity(back);
        finish();
    }
}