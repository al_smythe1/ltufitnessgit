package com.example.ltufitness1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    DBHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new DBHandler(this);
        SQLiteDatabase BS= handler.getWritableDatabase();
        handler.onCreate(BS);
    }

    public void signupButton(View view){
        Intent signupSwitch = new Intent(this, signup_activity.class);
        startActivity(signupSwitch);
    }
    public void loginButton(View view){
        EditText getlogin = findViewById(R.id.app_main_editbox_emailbox);
        EditText getpassword = findViewById(R.id.app_main_edittext_password);
        Log.d("auth","checking buttons for email");
        if (String.valueOf(getlogin.getText()).equals("")){
            getlogin.setError("Please Enter Email");
            getlogin.requestFocus();
            return;
        }
        Log.d("auth","checking buttons for password");
        if (String.valueOf(getpassword.getText()).equals("")){
            getpassword.setError("Please Enter Password");
            getpassword.requestFocus();
            return;
        }
        String textlogin = String.valueOf(getlogin.getText());
        String textpassword = String.valueOf(getpassword.getText());
        Log.d("auth","checking buttons if user exists ");
        if (handler.checkUserExists(textlogin,textpassword)){
            Intent home = new Intent(this,HomeActivity.class);
            home.putExtra("username",textlogin);
            startActivity(home);
            Log.d("auth","user exists logging in");
            finish();
        }
        else{
            Toast.makeText(this,"Email or Password is incorrect",Toast.LENGTH_LONG).show();
        }
    }

    public void logButton(View view){
        Intent loginswitch = new Intent(this,HomeActivity.class);
        startActivity(loginswitch);
    }
}