package com.example.ltufitness1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;

public class ProfileActivity extends AppCompatActivity {
    DBHandler handler;
    String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent accountintent = getIntent();
        username = accountintent.getStringExtra("username");
        handler = new DBHandler(this);
        SQLiteDatabase database = handler.getWritableDatabase();
        handler.onCreate(database);
    }
    public void deleteAccount(View view){
        handler.removeUserAccount(username);
        Intent deleteSwitch = new Intent(this,MainActivity.class);
        deleteSwitch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(deleteSwitch);
        finish();
    }
    public void backButton(View view){
        Intent back = new Intent(this,HomeActivity.class);
        startActivity(back);
        finish();
    }
}